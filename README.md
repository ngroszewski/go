# go

Install and manage Golang

## Dependencies

* [polkhan.asdf](https://gitlab.com/polkhan/asdf.git)
  _The asdf configs must be included in the users `playbook.yml` since no
  default configs are passed to the dependent role_

## Role Variables

* `versions`
    * Type: List
    * Usages: A list of Golang versions to install

* `global_version`
    * Type: String
    * Usages: The Golang version to make the global default

```
golang:
  versions:
    - 1.14
    - 1.13
  global_version: 1.13
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polkhan.go

## License

MIT
